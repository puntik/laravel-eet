<?php

namespace Abetzi\Eet;

class DataBuilder
{

	private const TAX_LEVEL_1 = 1;
	private const TAX_LEVEL_2 = 2;
	private const TAX_LEVEL_3 = 3;

	private $data = [];

	public function __construct($dic, $workshop, $cashRegister)
	{
		$this->data = [
			'dic_popl'  => $dic,
			'id_pokl'   => $cashRegister,
			'id_provoz' => $workshop,
		];
	}

	/*
	 * Ordering
	 */

	public function ordering(string $ordering): self
	{
		$this->data['porad_cis'] = $ordering;

		return $this;
	}

	/*
	 * First/next sending
	 */

	public function first(bool $first = true): self
	{
		$this->data['prvni_zaslani'] = $first ? 'true' : 'false';

		return $this;
	}

	public function next(): self
	{
		$this->first(false);

		return $this;
	}

	/*
	 * Dates
	 */

	public function now(): self
	{
		return $this->dateOfSale();
	}

	public function dateOfSale(int $timestamp = null): self
	{
		$this->data['dat_trzby'] = $timestamp === null ? date('c') : date('c', $timestamp);

		return $this;
	}

	/*
	 * System settings
	 */

	public function online(bool $regime = true): self
	{
		$this->data['rezim'] = $regime ? '1' : '0';

		return $this;
	}

	public function offline(): self
	{
		return $this->online(false);
	}

	/*
	 * Recipe information
	 */

	public function total(string $total, string $noVat = null): self
	{
		$this->validatePrice($total);

		$this->data['celk_trzba'] = $total;

		if ($noVat !== null) {
			$this->validatePrice($noVat);

			$this->noVat($noVat);
		}

		return $this;
	}

	public function noVat(string $base): self
	{
		$this->validatePrice($base);

		$this->data['zakl_nepodl_dph'] = $base;

		return $this;
	}

	public function used1(string $price): self
	{
		$this->validatePrice($price);

		return $this->used(self::TAX_LEVEL_1, $price);
	}

	public function used2(string $price): self
	{
		$this->validatePrice($price);

		return $this->used(self::TAX_LEVEL_2, $price);
	}

	public function used3(string $price): self
	{
		$this->validatePrice($price);

		return $this->used(self::TAX_LEVEL_3, $price);
	}

	private function used(int $level, string $price): self
	{
		$this->data['pouzit_zboz' . $level] = $price;

		return $this;
	}

	public function tax1(string $base, string $tax): self
	{
		return $this->tax(self::TAX_LEVEL_1, $base, $tax);
	}

	public function tax2(string $base, string $tax): self
	{
		return $this->tax(self::TAX_LEVEL_2, $base, $tax);
	}

	public function tax3(string $base, string $tax): self
	{
		return $this->tax(self::TAX_LEVEL_3, $base, $tax);
	}

	private function tax(int $level, string $base, string $tax): self
	{
		$this->validatePrice($base);
		$this->validatePrice($tax);

		$this->data['zakl_dan' . $level] = $base;
		$this->data['dan' . $level]      = $tax;

		return $this;
	}

	/*
	 * Business trip
	 */

	public function trip(string $base): self
	{
		$this->validatePrice($base);

		$this->data['cest_sluz'] = $base;

		return $this;
	}

	/*
	 * Create data
	 */

	public function toArray()
	{
		// mandatory fields

		if (! isset($this->data['celk_trzba'])) {
			throw new \InvalidArgumentException('Please fill the field "celk_trzba" as it is mandatory.');
		}

		if (! isset($this->data['porad_cis'])) {
			throw new \InvalidArgumentException('Please fill the field "porad_cis" as it is mandatory.');
		}

		// some check

		if (! isset($this->data['rezim'])) {
			$this->online();
		}

		if (! isset($this->data['prvni_zaslani'])) {
			$this->first();
		}

		if (! isset($this->data['dat_trzby'])) {
			$this->now();
		}

		// voila ..

		return $this->data;
	}

	/*
	 * Helpers
	 */

	private function validatePrice(string $price)
	{
		// data should be in format xxx.xx -> ^\d\.\d{2}$
		if (! preg_match('/^\d+\.\d{2}$/', $price)) {
			throw new \InvalidArgumentException('Price data should be in decimal format (/^\d+\.\d{2}$/).');
		}
	}
}
