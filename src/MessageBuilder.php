<?php declare(strict_types = 1);

namespace Abetzi\Eet;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;

class MessageBuilder
{

	/** @var string */
	private $pkcs12file;

	/** @var string */
	private $password;

	/** @var Client */
	private $httpClient;

	public function __construct(string $pkcs12file, string $password, bool $test = false)
	{
		$this->pkcs12file = $pkcs12file;
		$this->password   = $password;

		$this->httpClient = new Client([
			'base_uri' => sprintf('https://%s.eet.cz/eet/services/EETServiceSOAP/', $test ? 'pg' : 'prod'),
			'headers'  => [
				'Content-Type' => 'text/xml;charset=UTF-8',
				'SOAPAction'   => 'http://fs.mfcr.cz/eet/OdeslaniTrzby',
			],
		]);
	}

	public function send(array $data): array
	{
		$body = $this->buildRequest($data);

		try {
			$response = $this->httpClient->post('v3', [
				'body' => $body,
			]);

			// @todo parsovat na chyby
			return $this->parseResponse($response->getBody()->getContents());
		} catch (ClientException | ServerException $e) {
			// do nothing
		}

		throw new InvalidArgumentException('I could send or parse eet request.');
	}

	public function buildRequest(
		array $data
	): string {
		// Validate extensions
		if (! extension_loaded('openssl') || ! function_exists('openssl_pkcs12_read')) {
			throw  new InvalidArgumentException('Rozsireni OpenSSL neni dostupne.');
		}

		// parse pkc file
		$certInfo = $this->parsepk12($this->pkcs12file, $this->password);

		// sestav tělo soap zprávy
		$data["soap_body"] = $this->buildSoapBody($data, $certInfo['key']);

		// vypočti digest
		$data["digest"] = base64_encode(hash('sha256', $data["soap_body"], true));

		// sestav podpisovou část, podepiš a finalizuj soap request
		return $this->buildSoapSignature($data, $certInfo['key'], $certInfo['certb64']);
	}

	private function parseResponse($xml): array
	{
		$soap = simplexml_load_string($xml);

		$hlavicka  = $soap->xpath('//soapenv:Body/eet:Odpoved/eet:Hlavicka')[0];
		$potvrzeni = $soap->xpath('//soapenv:Body/eet:Odpoved/eet:Potvrzeni')[0];

		return [
			'dat_prij'    => (string) $hlavicka['dat_prij'],
			'uuid_zpravy' => (string) $hlavicka['uuid_zpravy'],
			'bkp'         => (string) $hlavicka['bkp'],
			'fik'         => (string) $potvrzeni['fik'],
			'test'        => (string) $potvrzeni['test'] === 'true',
		];
	}

	private function buildSoapSignature(array $data, $eetKey, $eetCert)
	{
		$data["certb64"] = $eetCert;

		$signatureTemplate = '<ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"><ec:InclusiveNamespaces xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="soap"></ec:InclusiveNamespaces></ds:CanonicalizationMethod><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod><ds:Reference URI="#TheBody"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod><ds:DigestValue>${digest}</ds:DigestValue></ds:Reference></ds:SignedInfo>';

		$signatureFinal = $this->replaceData($signatureTemplate, $data);

		openssl_sign($signatureFinal, $signature, $eetKey, OPENSSL_ALGO_SHA256);

		$data["signature"] = base64_encode($signature);

		$requestTemplate = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="1"><wsse:BinarySecurityToken EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3" wsu:Id="TheCert">${certb64}</wsse:BinarySecurityToken><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="TheSignature">' . $signatureFinal . '<ds:SignatureValue>${signature}</ds:SignatureValue><ds:KeyInfo Id="TheKeyInfo"><wsse:SecurityTokenReference wsu:Id="TheSecurityTokenReference"><wsse:Reference URI="#TheCert" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3"/></wsse:SecurityTokenReference></ds:KeyInfo></ds:Signature></wsse:Security></SOAP-ENV:Header>${soap_body}</soap:Envelope>';

		return $this->replaceData($requestTemplate, $data);
	}

	private function buildSoapBody(array $data, string $eetKey): string
	{
		$bodyTemplate = '<soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="TheBody" xml:id="TheBody"><Trzba xmlns="http://fs.mfcr.cz/eet/schema/v3"><Hlavicka @{dat_odesl} @{overeni} @{prvni_zaslani} @{uuid_zpravy}></Hlavicka><Data @{celk_trzba} @{cerp_zuct} @{cest_sluz} @{dan1} @{dan2} @{dan3} @{dat_trzby} @{dic_popl} @{dic_poverujiciho} @{id_pokl} @{id_provoz} @{porad_cis} @{pouzit_zboz1} @{pouzit_zboz2} @{pouzit_zboz3} @{rezim} @{urceno_cerp_zuct} @{zakl_dan1} @{zakl_dan2} @{zakl_dan3} @{zakl_nepodl_dph}></Data><KontrolniKody><pkp cipher="RSA2048" digest="SHA256" encoding="base64">${pkp}</pkp><bkp digest="SHA1" encoding="base16">${bkp}</bkp></KontrolniKody></Trzba></soap:Body>';

		// generuj datumy
		$data["dat_odesl"] = date("c");

		if (empty($data["dat_trzby"])) {
			$data["dat_trzby"] = date("c");
		}

		if (empty($data["uuid_zpravy"])) {
			$data["uuid_zpravy"] = Uuid::uuid4()->toString();
		}

		// vypočti PKP a BKP
		$pkpInput = $data["dic_popl"] . '|' . $data["id_provoz"] . '|' . $data["id_pokl"] . '|' . $data["porad_cis"] . '|' . $data["dat_trzby"] . '|' . $data["celk_trzba"];
		openssl_sign($pkpInput, $signature, $eetKey, OPENSSL_ALGO_SHA256);
		$data["pkp"] = base64_encode($signature);
		$data["bkp"] = $this->formatBkp($signature);

		// sestav tělo soap zprávy
		return $this->replaceData($bodyTemplate, $data);
	}

	/**
	 * @param string $pkcs12file
	 * @param string $password
	 *
	 * @return array
	 * @throws \Exception
	 */
	private function parsepk12(string $pkcs12file, string $password): array
	{
		$output = [];

		$pkcs12 = file_get_contents($pkcs12file);

		if (! extension_loaded('openssl') || ! function_exists('openssl_pkcs12_read')) {
			throw new \Exception('Rozsireni OpenSSL neni dostupne.');
		}

		$openSSL = openssl_pkcs12_read($pkcs12, $certs, $password);
		if (! $openSSL) {
			throw new \Exception('Certifikat se nepodarilo vyexportovat.');
		}

		$output['key']  = $certs['pkey'];
		$output['cert'] = $certs['cert'];

		$tmp = $certs['cert'];
		$tmp = explode('CERTIFICATE-----', $tmp);
		$tmp = explode('-----END', $tmp[1]);

		$output['certb64'] = $tmp[0];

		return $output;
	}

	private function formatBkp(string $code): string
	{
		$code = sha1($code);

		$output = '';
		for ($i = 0; $i < 40; $i++) {
			if ($i % 8 == 0 && $i != 0) {
				$output .= '-';
			}
			$output .= $code[$i];
		}

		return $output;
	}

	private function replaceData($template, $data)
	{
		foreach ($data as $key => $value) {
			$template = str_replace("\${" . $key . "}", $value, $template);
			$template = str_replace("@{" . $key . "}", "$key=\"$value\"", $template);
		}
		// odstraň prázdná pole
		$template = preg_replace("/\\$\\{[a-z_0-9:]+\\}/", "", $template);
		$template = preg_replace("/ @\\{[a-z_0-9:]+\\}/", "", $template);

		return $template;
	}
}
