![PHP from Packagist](https://img.shields.io/packagist/php-v/abetzi/eet-laravel)
[![Latest Stable Version](https://poser.pugx.org/abetzi/eet-laravel/v/stable)](https://packagist.org/packages/abetzi/eet-laravel)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/puntik/laravel-eet/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/puntik/laravel-eet/?branch=master)
[![Build Status](https://scrutinizer-ci.com/b/puntik/laravel-eet/badges/build.png?b=master)](https://scrutinizer-ci.com/b/puntik/laravel-eet/build-status/master)
[![Total Downloads](https://poser.pugx.org/abetzi/eet-laravel/downloads)](https://packagist.org/packages/abetzi/eet-laravel)

### usage
konfigurace do .env

    EET_DIC=CZ1212121218
    EET_WORKSHOP=001
    EET_CACHEREGISTER=prizemi_01
    EET_PASSWORD=eet
    EET_TEST_ENVIRONMENT=true

konfigurace (soubor config/eet.php)
~~~php
<?php
return [
    'dic'              => env('EET_DIC'),                    // DIC provozovatele
    'workshop'         => env('EET_WORKSHOP'),               // identifikace provozovny
    'cacheRegister'    => env('EET_CACHEREGISTER'),          // identifikace pokladny
    'password'         => env('EET_PASSWORD'),               // heslo pkcs12 souboru 
    'test_environment' => env('EET_TEST_ENVIRONMENT', false) // identifikace prostredi - true odesle vsechny podani na playground
];
~~~

nastavení provideru v laravelu: 
~~~php
<?php
$this->app->bind(MessageBuilder::class, function (): MessageBuilder {
    // prepare certificate
    $pkcs12file = storage_path('app/pkcs/EET_CA1_Playground-CZ1212121218.p12');
    $password   = config('eet.password', 'eet');
    $test       = config('eet.test_environment', true);

    return new MessageBuilder($pkcs12file, $password, $test);
});

$this->app->bind(DataBuilder::class, function (): DataBuilder {
    return new DataBuilder(
        config('eet.dic', 'CZ1212121218'),
        config('eet.workshop', '001'),
        config('eet.cacheRegister', 'prizemi_01'));
});
~~~

příklad použití commandline
~~~php
<?php
$this->dataBuilder
    ->ordering('1') // poradi zpravy z daneho zarizeni
    ->first()       // prvni zaslani
    ->now()         // cas odeslani
    ->total('100'); // celkova suma bez dph

$info = $this->messageBuilder->send($this->dataBuilder->toArray());
~~~

info obsahuje pole s informacema (příklad:):
~~~php
<?php
$info = [
  'dat_prij'    => '2020-02-28T14:32:08+01:00',
  'uuid_zpravy' => '061611d6-8b02-40b2-aefb-df0263d8d12f',
  'bkp'         => '0284f61e-9326b348-851eaa97-3bb6ad3d-0807a458',
  'fik'         => 'a0e20422-816e-4fba-930b-46bcf8eff5e9-fa',
  'test'        => true,
];
~~~

### todos (prevest na issues)
- dodelat databuilder a navazujici message builder
    - doladit zadavani dat pro dph (metody tax{1,2,3})
    - 'dic_poverujiciho' => "?",
    - 'urceno_cerp_zuct'=>"?"
    - 'cerp_zuct'=>"?"
- parametrizovat pkcs12 soubor, vyresit jeho ulozeni a nacitani
- finalizovat laravelizaci - automaticke nacteni provideru a konfigurace po composer requite
- dependencies (casem se jich zbavit - alespon ten carbon):
    - use GuzzleHttp\Client;
    - use GuzzleHttp\Exception\ClientException;
    - use GuzzleHttp\Exception\ServerException;
    - use Ramsey\Uuid\Uuid;
- nejake testy


