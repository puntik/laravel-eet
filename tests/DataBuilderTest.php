<?php

use Abetzi\Eet\DataBuilder;
use PHPUnit\Framework\TestCase;

class DataBuilderTest extends TestCase
{

	/** @var DataBuilder */
	private $builder;

	protected function setUp(): void
	{
		// Set basic data builder - vat id coresponds with test
		$this->builder = new DataBuilder('CZ1212121218', '0101', 'prizemi_01');

		parent::setUp();
	}

	/**
	 * @test
	 */
	public function it_fails_when_ordering_field_is_not_set()
	{
		// Given
		$this->builder
			->total('100.00');

		// Then
		$this->expectException(InvalidArgumentException::class);

		// Provide command
		$this->builder->toArray();
	}

	/**
	 * @test
	 */
	public function it_fails_when_total_field_is_not_set()
	{
		// Given
		$this->builder
			->ordering('faktura_2002');

		// Then
		$this->expectException(InvalidArgumentException::class);

		// Provide command
		$this->builder->toArray();
	}

	/**
	 * @test
	 */
	public function it_works_for_no_vat_payer()
	{
		// Given
		$this->builder->ordering('faktura_2002')
		              ->total('100.00');

		// When
		$eetData = $this->builder->toArray();

		// Then
		$this->assertArrayHasKey('porad_cis', $eetData);
		$this->assertArrayHasKey('celk_trzba', $eetData);
	}

}
