<?php

use Abetzi\Eet\DataBuilder;
use Abetzi\Eet\MessageBuilder;
use PHPUnit\Framework\TestCase;

class MessageBuilderTest extends TestCase
{

	/** @var MessageBuilder */
	private $messageBuilder;

	/** @var DataBuilder */
	private $dataBuilder;

	protected function setUp(): void
	{
		// set test message builder
		$pkcs12file = __DIR__ . '/_data/EET_CA1_Playground-CZ1212121218.p12';
		$password   = 'eet';

		$this->messageBuilder = new MessageBuilder($pkcs12file, $password, true);

		// Set basic data builder - vat id coresponds with test
		$this->dataBuilder = new DataBuilder('CZ1212121218', '0101', 'prizemi_01');

		parent::setUp();
	}

	/**
	 * @test
	 */
	public function it_sends_a_request_to_mfcr_server_and_parse_response()
	{
		// Given
		$data = $this->dataBuilder->ordering('faktura_001')->total('100.00');

		// When
		$response = $this->messageBuilder->send($data->toArray());

		// Than
		$this->assertArrayHasKey('dat_prij', $response);
		$this->assertArrayHasKey('fik', $response);
		$this->assertArrayHasKey('uuid_zpravy', $response);
		$this->assertArrayHasKey('test', $response);
		$this->assertArrayHasKey('bkp', $response);
	}
}
